"""
Run only for the first time
"""

import mysql.connector
from main import app, db

with open("pwd.txt", "r") as f:
    pwd = f.read()

mydb = mysql.connector.connect(host="localhost", user="root", password=pwd, auth_plugin='mysql_native_password')
my_cursor = mydb.cursor()
my_cursor.execute("CREATE DATABASE simpleFlaskApp")

with app.app_context():
    db.create_all()

