from dataclasses import dataclass
from flask import Flask, jsonify, request, Response, abort
from flask_sqlalchemy import SQLAlchemy

with open("pwd.txt", "r") as f:
    pwd = f.read()

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql+pymysql://root:{pwd}@localhost/simpleFlaskApp' # doesn't work without pymysql
db = SQLAlchemy(app)


@dataclass
class Post(db.Model):
    id: int
    title: str
    content: str

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50), unique=True, nullable=False)
    content = db.Column(db.String(200), unique=True, nullable=False)

    def __repr__(self):
        return "{" + f'"id": {self.id}, "title": "{self.title}", "content": "{self.content}"' + "}"


@app.route('/api/blog', methods=['GET']) # all posts
def index():
    return jsonify(Post.query.all())

@app.route("/api/blog/<int:post_id>", methods=["GET", "PUT", "DELETE"])
def post_details(post_id):
    post = Post.query.get(post_id)
    if post:
        if request.method == "GET":
            return jsonify(post)
        elif request.method == "PUT":
            if request.json["title"]:
                post.title = request.json["title"]
            if request.json["content"]:
                post.content = request.json["content"]
            post.verified = True
            db.session.commit()
            return {"message": f"Post with id {post.id} was succesfully updated"}
        elif request.method == "DELETE":
            db.session.delete(post)
            db.session.commit()
            return {"message": f"Post with id {post.id} was succesfully deleted"}
    else:
        abort(404)

@app.route('/api/blog/new', methods=["POST"])
def publish():
    post = Post(
        title=request.json["title"],
        content=request.json["content"],
    )
    db.session.add(post) 
    db.session.commit()
    return {"message": f"Post succesfully created, id: {post.id}"}


if __name__=="__main__":
    app.run()

